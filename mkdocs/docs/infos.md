# Analyse

### **Comment fonctionne l'application ?**

Au lancement de l'application, un objet baignoire est créé. Deux threads sont lancés :

* Un concernant le remplissage de la baignoire, s'exécutant toutes les secondes, en fonction de la valeur renseignée dans le champ "débit de remplissage"
* Un deuxième concernnant les fuites de la baignoire. S'exécutant à intervales aléatoires, la quantité d'eau dans la baignoire est diminuée de la somme de la taille de tous les trous.

---

### Interface homme-machine

###### Aspect global

Pour développer cette application, j'ai fait le choix d'un IHM plutôt simple. Il n'existe pas de bouton "démarrer" ni "arrêter" afin d'éviter une surchage pour l'utilisateur. Cela implique peut d'actions possibles, liées à la baignoire, ce qui facilite l'utilisation.

###### UX

Lors de l'utilisation de l'application, des fenêtres pop-up peuvent apparaître. Elles permettent à l'utilisateur d'être informé lorsque les données sont en cours de téléchargement, ou alors lorsque des erreurs sont commises par lui *(lors de l'insertion de mauvaises valeurs par exemple)*
