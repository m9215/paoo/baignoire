# Librairies utilisées

Toutes les librairies (ou dépendences) sont importées à partir du logiciel [Maven](https://maven.apache.org/).

## Liste

* **JAVA FX** : utilisée pour le design global de l'application. [*Plus d'information...
