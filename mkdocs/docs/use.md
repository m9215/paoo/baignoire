# Manuel d'utilisation

Pour utiliser l'application, rien de plus simple. Il suffit juste de remplir les différents champs, et d'ajouter des trous si l'utilisateur le souhaite. Tout au long du remplissage, les données se mettent à jour automatiquement. A la fin du remplissage de la baignoire, un message d'affiche. Pour remettre à 0 l'application, il suffit d'appuyer sur le bouton "Stop/Reset".

---

### **Ajouter/Supprimer des trous** :

Il est possible d'ajouter un trou en appuyant sur le bouton "Ajouter un trou". Comme pour le débit de remplissage, il faut renseigner un débit entre 1 et 10 (un trou ne peut pas ne rien faire couler hihi). Il est aussi possible de supprimer un trou en le sélectionnant, ou de supprimer tous les trous.

![Screenshot_70.png](./assets/Screenshot_70.png)
