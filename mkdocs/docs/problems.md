# Problèmes rencontrés

Je n'ai pas rencontré beaucoup de problèmes durant le développement de cette application, mis à part un : lorsque que je compilais le projet pour générer le fichier bat. Au départ, je stockais les différentes images de baignoire dans le dossier "samples". Après question au professeur, je les ais classées en tant que ressources, et mon problème était réglé.

## Petit plus

Mon application est aussi designée de telle sorte à ce que il est possible de se passer de boutons, tout simplement en déplaçant le démarrage des Threads. Ainsi, les modifications se feront dynamiquement.
