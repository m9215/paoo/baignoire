module m.paoo.tp.baignoire {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.logging;

    opens m1.paoo.tp2.baignoire to javafx.fxml;
    exports m1.paoo.tp2.baignoire;
}