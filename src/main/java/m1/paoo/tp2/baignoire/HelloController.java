package m1.paoo.tp2.baignoire;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import m1.paoo.tp2.baignoire.objects.Bathtub;
import m1.paoo.tp2.baignoire.objects.Evacuation;
import m1.paoo.tp2.baignoire.objects.Faucet;
import m1.paoo.tp2.baignoire.objects.Hole;

import java.net.URL;
import java.util.Optional;
import java.util.logging.Logger;

public class HelloController {
    private static final Logger LOG = Logger.getLogger(HelloController.class.getName());
    private long startTime;

    @FXML
    private ListView<Hole> holeList;

    @FXML
    private ImageView bathtubImage;

    @FXML
    private Label fillPercentage, waterIn, waterOut, waterRate, timing;

    @FXML
    private TextField fillingRateLabel;

    @FXML
    private TextField maxFillBathtub;
    @FXML
    private Button buttonStopButton, buttonStartButton, addHoleButton, removeHoleButton, removeAllHolesButton;

    private final Bathtub bathtub = new Bathtub();

    private Faucet faucet = new Faucet(bathtub);
    private Evacuation evacuation = new Evacuation(bathtub);

    private void initializeThreads() {
        faucet = new Faucet(bathtub);
        evacuation = new Evacuation(bathtub);

        faucet.setOnSucceeded(event -> {
            bathtub.setWaterIn(bathtub.getWaterIn() + bathtub.getFillingRate());
            bathtub.setWaterOut(bathtub.getWaterOut() + bathtub.getHolesSize());
            waterIn.setText("Quantité d'eau totale entrée dans la baignoire : " + bathtub.getWaterIn() + "L");
            waterOut.setText("Quantité d'eau totale sortie de la baignoire : " + bathtub.getWaterOut() + "L ");
            timing.setText("Temps écoulé : " + ((System.nanoTime() - startTime)/1000000000) + "s");
            if (bathtub.getWaterIn() > 0) {
                waterRate.setText("Taux de fuite : "+bathtub.getWaterOut()*100/ bathtub.getWaterIn()+"%");
            } else {
                waterRate.setText("Taux de fuite : 0%");
            }

            this.changeBathtubDatas();

            if (bathtub.isFull()) {
                faucet.cancel();
                evacuation.cancel();
                showAlert("Baignoire pleine", "Baignoire pleine", "La baignoire est pleine", Alert.AlertType.INFORMATION);
            }
        });

        faucet.setOnFailed(event -> {
            LOG.severe("Faucet failed");
            showAlert("Erreur", "Fonctionnement du robinet", "Le robinet a échoué", Alert.AlertType.ERROR);
        });


        evacuation.setOnSucceeded(event -> {
            this.changeBathtubDatas();
        });
        evacuation.setOnFailed(event -> {
            LOG.severe("Evacuation failed");
            showAlert("Erreur", "Fonctionnement de l'évacuation", "L'évacuation a échoué", Alert.AlertType.ERROR);
        });
    }

    @FXML
    public void initialize() {
        this.changeBathtubDatas();
        fillingRateLabel.setText(String.valueOf(bathtub.getFillingRate()));
        initializeThreads();
        LOG.info("Start application");

    }

    @FXML
    public void buttonStart() {
        startTime = System.nanoTime();
        faucet.start();
        evacuation.start();
        buttonStartButton.setDisable(true);
        buttonStopButton.setDisable(false);
        addHoleButton.setDisable(true);
        removeHoleButton.setDisable(true);
        removeAllHolesButton.setDisable(true);
        holeList.setDisable(true);
        maxFillBathtub.setDisable(true);
        fillingRateLabel.setDisable(true);
        waterIn.setText("Quantité d'eau totale entrée dans la baignoire : " + bathtub.getWaterIn() + "L");
        waterOut.setText("Quantité d'eau totale sortie de la baignoire : " + bathtub.getWaterOut() + "L ");
        waterRate.setText("Taux de fuite : 0%");
        timing.setText("Temps écoulé : 0s");
    }

    @FXML
    public void buttonStop() {
        bathtub.reset();
        fillingRateLabel.setText("0");
        maxFillBathtub.setText("0");
        changeBathtubDatas();
        faucet.cancel();
        evacuation.cancel();
        buttonStartButton.setDisable(false);
        buttonStopButton.setDisable(true);
        addHoleButton.setDisable(false);
        removeHoleButton.setDisable(false);
        removeAllHolesButton.setDisable(false);
        holeList.setDisable(false);
        maxFillBathtub.setDisable(false);
        fillingRateLabel.setDisable(false);
        initializeThreads();
    }

    private void changeBathtubDatas() {
        // change image
        String name = "images/baignoire-" + bathtub.getFillingImage() + ".png";
        Image image = null;
        try {
            URL url = getClass().getResource(name);
            image = new Image(String.valueOf(url));
        } catch (Exception e) {
            LOG.severe("Image not found");
            showAlert("Erreur", "Image non trouvée", "L'image " + name + " n'a pas été trouvée", Alert.AlertType.ERROR);
        }
        bathtubImage.setImage(image);

        // change percentage
        if (bathtub.getMaxFilling() != 0) {
            fillPercentage.setText(bathtub.getFilling()*100/bathtub.getMaxFilling() + " %");
        } else {
            fillPercentage.setText("0 %");
        }
    }

    @FXML
    public void showAddHoleModal() {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Ajouter un trou");
        dialog.setHeaderText("Quelle taille le trou doit avoir ?");
        dialog.setContentText("Taille : ");

        int size;
        Optional<String> result = dialog.showAndWait();
        try {
            size = result.map(Integer::parseInt).orElse(-1);
        } catch (Exception e) {
            LOG.severe("Invalid input");
            showAlert("Erreur", "Mauvais format", "Veuillez entrer un nombre entier", Alert.AlertType.ERROR);
            return;
        }
        if (size < 1 || size > 10) {
            LOG.info("Size must be between 0 and 10");
            showAlert("Information", "Valeur incorrecte", "La taille doit être entre 1 et 10", Alert.AlertType.INFORMATION);
            return;
        }
        addHole(size);
    }

    @FXML
    public void deleteSelectedHole() {
        Hole hole = holeList.getSelectionModel().getSelectedItem();
        if (hole != null) {
            bathtub.removeHole(hole);
            holeList.getItems().remove(hole);
            holeList.refresh();
        }
    }

    @FXML
    public void deleteAllHoles() {
        bathtub.removeAllHoles();
        holeList.getItems().clear();
        holeList.refresh();
    }

    public void addHole(int size) {
        Hole hole = new Hole(size);
        bathtub.addHole(hole);
        holeList.getItems().add(hole);
        holeList.refresh();
    }

    private void showAlert(String title, String header, String content, Alert.AlertType alertType) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.showAndWait();
    }

    @FXML
    public void changeFillingRate() {
        if (fillingRateLabel.getText().isEmpty()) {
            bathtub.setFillingRate(0);
            return;
        }
        int fillingRate;
        try {
            fillingRate = Integer.parseInt(fillingRateLabel.getText());
        } catch (Exception e) {
            LOG.severe("Invalid input");
            showAlert("Erreur", "Mauvais format", "Veuillez entrer un nombre entier", Alert.AlertType.ERROR);
            bathtub.setFillingRate(bathtub.getFillingRate());
            fillingRateLabel.setText(String.valueOf(bathtub.getFillingRate()));
            return;
        }
        if (fillingRate < 0 || fillingRate > 10) {
            LOG.info("Filling rate must be between 0 and 10");
            showAlert("Information", "Valeur incorrecte", "La valeur doit être entre 0 et 10", Alert.AlertType.INFORMATION);
            if (fillingRate > 10) {
                bathtub.setFillingRate(10);
                fillingRateLabel.setText("10");
            }
            return;
        }
        bathtub.setFillingRate(fillingRate);
    }

    @FXML
    public void changeMaxFill() {
        if (maxFillBathtub.getText().isEmpty()) {
            bathtub.setMaxFilling(0);
            return;
        }
        int max;
        try {
            max = Integer.parseInt(maxFillBathtub.getText());
        } catch (Exception e) {
            LOG.severe("Invalid input");
            showAlert("Erreur", "Mauvais format", "Veuillez entrer un nombre entier", Alert.AlertType.ERROR);
            bathtub.setMaxFilling(bathtub.getFillingRate());
            maxFillBathtub.setText(String.valueOf(bathtub.getFillingRate()));
            return;
        }
        if (max < 0 || max > 1000) {
            LOG.info("Filling rate must be between 0 and 10");
            showAlert("Information", "Valeur incorrecte", "La valeur doit être entre 0 et 1000", Alert.AlertType.INFORMATION);
            if (max > 1000) {
                bathtub.setMaxFilling(1000);
                maxFillBathtub.setText("1000");
            }
            return;
        }
        bathtub.setMaxFilling(max);
    }
}