package m1.paoo.tp2.baignoire.objects;

public class Hole {

    private int size;

    public Hole(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    // to string
    @Override
    public String toString() {
        return " → " + size;
    }
}
