package m1.paoo.tp2.baignoire.objects;

import java.util.ArrayList;
import java.util.logging.Logger;

public class Bathtub {

    private static final Logger LOG = Logger.getLogger(Bathtub.class.getName());


    private int filling;
    private int fillingRate;
    private ArrayList<Hole> holes;
    private int waterIn;
    private int waterOut;

    private int maxFilling;

    public Bathtub() {
        this.filling = 0;
        this.fillingRate = 0;
        this.maxFilling = 0;
        this.waterIn = 0;
        this.waterOut = 0;
        this.holes = new ArrayList<>();
    }

    // add hole
    public void addHole(Hole hole) {
        this.holes.add(hole);
    }

    // remove hole
    public void removeHole(Hole hole) {
        this.holes.remove(hole);
    }

    public Integer fill() {
        if (this.filling + this.fillingRate > this.maxFilling) {
            this.filling = this.maxFilling;
            return this.filling;
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            LOG.info("InterruptedException : " + e.getMessage());
        }

        synchronized (this) {
            this.filling += this.fillingRate;
        }
        return this.filling;
    }

    // remove filling
    public Integer empty() {
        if (this.filling - getHolesSize() < 0) {
            this.filling = 0;
            return this.filling;
        }
        try {
            Thread.sleep((long)(Math.random() * 2000));
        } catch (InterruptedException e) {
            LOG.info("InterruptedException : " + e.getMessage());
        }
        synchronized (this) {
            this.filling -= getHolesSize();
        }
        return this.filling;
    }

    // get sum of the size of all holes
    public int getHolesSize() {
        int sum = 0;
        for (Hole hole : this.holes) {
            sum += hole.getSize();
        }
        return sum;
    }

    public int getFilling() {
        return filling;
    }

    public void setFilling(int filling) {
        this.filling = filling;
    }

    public ArrayList<Hole> getHoles() {
        return holes;
    }

    public void setHoles(ArrayList<Hole> holes) {
        this.holes = holes;
    }

    public void removeAllHoles() {
        this.holes.clear();
    }

    public String getFillingImage() {
        int fill = 0;
        int imageNumber = 0;
        if (this.filling == 0) {
            return "0";
        }
        while (fill < this.maxFilling) {
            if (fill > this.filling) {
                return String.valueOf(imageNumber);
            }
            fill += this.maxFilling / 7;
            imageNumber++;
        }
        return "8";
    }

    public void setFillingRate(int fillingRate) {
        this.fillingRate = fillingRate;
    }

    public int getFillingRate() {
        return fillingRate;
    }

    public int getMaxFilling() {
        return maxFilling;
    }

    public void setMaxFilling(int i) {
        this.maxFilling = i;
    }

    public int getWaterIn() {
        return waterIn;
    }

    public void setWaterIn(int waterIn) {
        this.waterIn = waterIn;
    }

    public int getWaterOut() {
        return waterOut;
    }

    public void setWaterOut(int waterOut) {
        this.waterOut = waterOut;
    }

    public boolean isFull() {
        return this.filling == this.maxFilling;
    }

    public void reset() {
        this.filling = 0;
        this.fillingRate = 0;
        this.maxFilling = 0;
        this.waterIn = 0;
        this.waterOut = 0;
    }
}
