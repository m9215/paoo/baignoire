package m1.paoo.tp2.baignoire.objects;

import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.util.Duration;

public class Evacuation extends ScheduledService<Integer> {

    private Bathtub bathtub;

    public Evacuation(Bathtub bathtub) {
        super();
        this.setPeriod(Duration.millis(500));
        this.bathtub = bathtub;
    }

    @Override
    protected Task<Integer> createTask() {
        return new Task<>() {
            @Override
            protected Integer call() {
                return bathtub.empty();
            }
        };
    }
}
