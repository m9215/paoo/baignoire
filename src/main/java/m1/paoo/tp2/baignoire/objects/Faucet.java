package m1.paoo.tp2.baignoire.objects;

import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;

public class Faucet extends ScheduledService<Integer> {
    private Bathtub bathtub;

    public Faucet(Bathtub bathtub) {
        super();
        this.bathtub = bathtub;
    }

    @Override
    protected Task<Integer> createTask() {
        return new Task<>() {
            @Override
            protected Integer call() {
                return bathtub.fill();
            }
        };
    }
}
